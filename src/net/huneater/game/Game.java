package net.huneater.game;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

public class Game extends Canvas implements Runnable {
    public static final int WIDTH = 320;
    public static final int HEIGHT = 240;
    public static final int SCALE = 2;

    private BufferedImage screenImg;
    private Bitmap screenBitmap;
    private boolean keepRunning = true;

    public Game() {
        Dimension size = new Dimension(WIDTH * SCALE, HEIGHT * SCALE);
        setPreferredSize(size);
        setMaximumSize(size);
        setMinimumSize(size);
    }

    public void start() {
        new Thread(this, "Game thread").start();
    }

    public void stop() {
        keepRunning = false;
    }

    @Override
    public void run() {
        init();

        while (keepRunning) {
            tick();
            render(screenBitmap);

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            swap();
        }
    }

    private void swap() {
        BufferStrategy bs = getBufferStrategy();
        if (bs == null) {
            this.createBufferStrategy(2);
            return;
        }

        Graphics g = bs.getDrawGraphics();
        int screenW = getWidth();
        int screenH = getHeight();
        int w = WIDTH * SCALE;
        int h = HEIGHT * SCALE;

        g.setColor(Color.BLACK);
        g.fillRect(0, 0, screenW, screenH);
        g.drawImage(screenImg, (screenW - w) / 2, (screenH - h) / 2, w * 20, h * 15, null);
        g.dispose();

        bs.show();
    }

    private int[] anim = {0, 2, 0, 3};
    private void render(Bitmap screen) {
        int frame = step / 10 % 4;
        screen.draw(Art.i.sprites[anim[frame]][0], 0, 0);
    }

    public int step = 0;
    private void tick() {
        step++;
    }

    private void init() {
        Art.init();

        screenImg = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_ARGB);
        screenBitmap = new Bitmap(screenImg);
    }

    public static void main(String[] args) {
        Game gameComponent = new Game();

        JFrame frame = new JFrame("Game");
        frame.add(gameComponent);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setVisible(true);

        gameComponent.start();
    }
}
